<?php
if(isset($_POST['email'])) {
	$email_to = "biuro@biurorachunkowe.legnica.pl";
	$email_subject = "Zapytanie - strona internetowa";
	function died($error) {
		echo "Przykro nam, ale niestety wystąpił problem z wypełnionym formularzem. ";
		echo "Błędy pojawią się poniżej.<br /><br />";
		echo $error."<br /><br />";
		echo "Prosimy powrócić i poprawić błędy.<br /><br />";
		die();
	}
	if(!isset($_POST['first_name']) ||
		!isset($_POST['email']) ||
		!isset($_POST['subject']) ||
		!isset($_POST['comments'])) {
		died('Przykro nam, ale niestety wystąpił problem z wypełnionym formularzem.');
	}
	$first_name = $_POST['first_name']; // wymagane
	$email_from = $_POST['email']; // wymagane
	$subject = $_POST['subject']; // nie wymagane
	$comments = $_POST['comments']; // wymagane
	$error_message = "";
	$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  if(!preg_match($email_exp,$email_from)) {
  	$error_message .= 'Adres e-mail nie wydaje się być poprawnym.<br />';
  }
	$string_exp = "/^[A-Za-ząćęłńóśżźĄĆĘŁŃÓŚŻŹ .'-]+$/";
  if(!preg_match($string_exp,$first_name)) {
  	$error_message .= 'Imię i nazwisko nie zostało poprawnie wpisane.<br />';
  }
  if(strlen($comments) < 2) {
  	$error_message .= 'Wpisana wiadomość jest zdecydowanie za krótka.<br />';
  }
  if(strlen($error_message) > 0) {
  	died($error_message);
  }
	$email_message = "Szczegóły wiadomości poniżej.\n\n";
	function clean_string($string) {
	  $bad = array("content-type","bcc:","to:","cc:","href");
	  return str_replace($bad,"",$string);
	}
	$email_message .= "Imię i nazwisko: ".clean_string($first_name)."\n";
	$email_message .= "E-mail: ".clean_string($email_from)."\n";
	$email_message .= "Temat wiadomości: ".clean_string($subject)."\n";
	$email_message .= "Wiadomość: ".clean_string($comments)."\n";
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);
?>
Dziękujemy za przesłanie wiadomości. Odpowiemy tak szybko jak to możliwe!
<?php
}
?>
